(function() {

  'use strict';

  var generator = require('yeoman-generator');
	var path = require('path');
	var fs = require('fs');
	var optionOrPrompt = require('yeoman-merge-prompt');

  var generatorConfig = {
		_optionOrPrompt: optionOrPrompt,
		constructor: constructor,
	  prompting: prompting,
		writing: writing,
    install: install
  };

	function constructor() {
		generator.Base.apply(this, arguments);
	}

  function prompting() {

		var done = this.async();

    this._optionOrPrompt([

      {
        type: 'input',
        message: 'Name (CamelCase without space): ',
        name: 'name',
        default: 'Project'
      },

      {
        type: 'input',
        message: 'Description: ',
        name: 'description',
        default: 'Just another project.'
      },

      {
        type: 'input',
        message: 'Version: ',
        name: 'applicationVersion',
        default: '0.0.1'
      },

      {
        type: 'list',
        message: 'Application type : ',
				choices: [
					{
						name: 'Mobile Hybrid',
						value: 'mobile_hybrid'
					},
					{
						name: 'Web',
						value: 'web'
					}
				],
        name: 'applicationType'
      }


    ], function(answers) {

      answers.basename = answers.name.toLowerCase();

			if(answers.applicationType == 'mobile_hybrid') {
				answers.distFolder = 'www';
			}else {
				answers.distFolder = 'dist';
			}

			answers.version = answers.applicationVersion;

			this.project = answers;
			done();

    }.bind(this));

  }

	function install() {
    this.bowerInstall();
    this.npmInstall();
  }

	function writing() {

		this.fs.copyTpl(
			this.templatePath('**/*'),
			this.destinationPath(),
			this.project
		);

		this.fs.copyTpl(
			this.templatePath('**/.*'),
			this.destinationPath(),
			this.project
		);

	}

  module.exports = generator.Base.extend(generatorConfig);

})();
