(function() {

  'use strict';

  angular
    .module('main', [
      'main.core'
    ]);

})();
