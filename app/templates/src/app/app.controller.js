(function() {

  'use strict';

  angular
    .module('main.app')
    .controller('AppController', AppController);

  AppController.$inject = ['$scope'];

  function AppController($scope) {

  }

})();
